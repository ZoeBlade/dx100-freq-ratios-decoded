# fm-ratio-tstr.py
# Python code to generate Addendum: √2, π/2, and √3, to 15x multiplication as part of the DX100 FREQ RATIOS DECODED repository
# You can use this code to generate new charts to higher resolution or larger multiplication factors for DX7 et al with larger FM ratios
# Dave Burraston June 2021, Zoë Blade December 2022
# www.noyzelab.com

squareRootOfTwo = 1.414
halfPi = 1.571
squareRootOfThree = 1.732
style = '{:5.2f}'

print("n\troot2\tpi/2\troot3")

n = 0.5
print(style.format(n), "\t", style.format(squareRootOfTwo * n), "\t", style.format(halfPi * n), "\t", style.format(squareRootOfThree * n))

for n in range(1, 16):
	print(style.format(n), "\t", style.format(squareRootOfTwo * n), "\t", style.format(halfPi * n), "\t", style.format(squareRootOfThree * n))
